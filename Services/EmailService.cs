﻿namespace WebApi.Services;

public class EmailService
{
    public StorageService StorageService { get; set; }
    public EmailService()
    {
        StorageService = new StorageService();
    }

    public void SendEmail(string email)
    {
        Console.WriteLine($"Sending email to {email}");
        StorageService.Add(email);
    }
}
